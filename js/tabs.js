/**
 * @author Raphael Müßeler
 */

if (typeof jQuery === "undefined") {
    throw new Error("jquery-confirm requires jQuery");
}

let Tabs;

(function () {
    $.fn.tabs = function(options) {
        if ($(this).length) {
            let tabs = new Tabs(this, options);

            $(this).find('li').each(function () {
                $(this).on('mousedown', function (event) {
                    tabs.onClick($(this), event);
                })
            });

            $(this).find('span.scroll-btn').each(function () {
                $(this).on('mousedown', function (event) {
                    let direction = ($(this).hasClass('glyphicon-chevron-left') ? 'left' : 'right');
                    tabs.scroll(direction, event);
                })
            });

            $(window).on('resize', function () {
                window.location.reload(false);
            });

            return this;
        }
    };

    Tabs = function (element, options) {
        this.element = element;

        let defaultOptions = {
            tabsBackground: 'none',
            contentBackground: 'none',
            activeColor: "#444",
            inactiveColor: '#BBB',
            animate: true,
            easing: 'easeInOutQuart',
            duration: 1500,
            // TODO: keyUsage
            keyUsage: true,
            useURL: true
        };

        this.options = $.extend({}, defaultOptions, options || {});

        this.isMobile = false;

        this.activeTabContent = null;
        this.activeTab = null;

        this.clickDisabled = false;

        this.init();
    };

    Tabs.prototype.constructor = Tabs;

    Tabs.prototype.init = function() {
        // set isMobile
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series([46])0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br([ev])w|bumb|bw-([nu])|c55\/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do([cp])o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly([-_])|g1 u|g560|gene|gf-5|g-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd-([mpt])|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c([- _agpst])|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac([ \-\/])|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja([tv])a|jbro|jemu|jigs|kddi|keji|kgt([ \/])|klon|kpt |kwc-|kyo([ck])|le(no|xi)|lg( g|\/([klu])|50|54|-[a-w])|libw|lynx|m1-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t([- ov])|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30([02])|n50([025])|n7(0([01])|10)|ne(([cm])-|on|tf|wf|wg|wt)|nok([6i])|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan([adt])|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk\/|se(c([-01])|47|mc|nd|ri)|sgh-|shar|sie([-m])|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel([im])|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c([- ])|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas-|your|zeto|zte-/i.test(navigator.userAgent.substr(0,4))) this.isMobile = true;

        // add css class tabs to ul
        if (!this.element.children('ul').hasClass('tabs')) {
            this.element.children('ul').addClass('tabs');
        }

        // append #slider to ul
        if (this.element.children("#slider").length <= 0) {
            this.element.children('ul').append('<li id = "slider"></li>');
        }

        // clear float
        if (this.element.children('div.clear-float').length <= 0) {
            this.element.append('<div class = "clear-float"></div>');
        }
        this.element.children('.tab-content:last-child').addClass('clear-float');

        // set tab active
        let url = window.location.href;
        if (this.options.useURL && url.indexOf('#') >= 0 && url.substr(url.indexOf('#')).length > 1) {
            // if tabId is given in URL
            this.setTabActive(this.element.find('li[data-tab-id=' + url.substr((url.indexOf('#') + 1)) + ']'));
        } else {
            this.setTabActive(this.element.find('li:first-child'));
        }

        this.setColors();

        // set containers height
        this.element.find('.tab-content-container').css('height', this.activeTabContent.height());


        // if tabs are bigger than containers width, add navigation buttons
        let realWidth = 0;

        this.element.find('ul').children('li').each(function() {
            if ($(this).attr('id') !== 'slider') {
                realWidth += $(this).width() + 60;
            }
        });

        if (this.element.find('ul').width() < realWidth) {
            this.element.find('ul.tabs').css('width', '-=100px');

            this.element.find('ul').before(
                '<span class = "scroll-btn glyphicon glyphicon-chevron-left disabled" id = "button-scroll-left"' +
                'style = "color: ' + this.options.inactiveColor + '; background: ' + this.options.tabsBackground + '; ' +
                'height: ' + this.element.find('ul.tabs').css('height') + '"></span>'
            );
            this.element.find('ul').after(
                '<span class = "scroll-btn glyphicon glyphicon-chevron-right" id = "button-scroll-right"' +
                'style = "color: ' + this.options.inactiveColor + '; background: ' + this.options.tabsBackground + '; ' +
                'height: ' + this.element.find('ul.tabs').css('height') + '"></span>'
            );

            this.scrollTo(this.activeTab);
        }

        // set sliders width
        this.element.find('#slider').css('width', (this.activeTab.width() + 60) + 'px');

        //set sliders position
        let pos = this.activeTab.offset().left - this.element.offset().left;
        if ($('#button-scroll-left').length) pos -= 50;
        this.element.find('#slider').css({
            'left': pos,
            'width': (this.activeTab.width() + 60) + 'px'
        });
    };

    Tabs.prototype.onClick = function (clickedTab, event) {
        if (!this.clickDisabled) {

            if (this.activeTab.attr('data-tab-id') !== clickedTab.attr('data-tab-id')) {
                this.clickDisabled = true;

                // move slider and change its width
                this.element.find('#slider').animate({
                    'left': this.getPositionOfTab(clickedTab) + 'px',
                    'width': (clickedTab.width() + 60) + 'px'
                }, {
                    duration: this.options.duration,
                    easing: this.options.easing,
                    complete: function () {

                    }
                });

                // show ripple
                this.ripple(clickedTab, event);

                let _activeTab = this.activeTab;

                this.setTabActive(clickedTab);

                // animate containers height
                this.element.find('.tab-content-container').animate({
                    'height': this.activeTabContent.css('height')
                }, {
                    duration: this.options.duration,
                    easing: this.options.easing
                });

                // scroll to tab
                this.scrollTo(clickedTab);


                let contentWidth = $('#' + clickedTab.attr('data-tab-id')).css('width');

                if (clickedTab.index() < _activeTab.index()) {
                    // if clickedTab is to the left of the activeTab

                    let self = this;

                    // animate activeTab to the right
                    $('#' + _activeTab.attr('data-tab-id')).animate({
                        left: contentWidth
                    }, {
                        duration: this.options.duration,
                        easing: this.options.easing,
                        complete: function() {
                            self.setTabsInactive(_activeTab);
                            self.clickDisabled = false;
                            $(this).css('left', '0');
                        }
                    });

                    // TODO: remove activeColor from old tab

                    // animate clickedTab to the right
                    $('#' + clickedTab.attr('data-tab-id')).css({
                        left: '-' + contentWidth
                    }).animate({
                        left: '0'
                    }, {
                        duration: this.options.duration,
                        easing: this.options.easing
                    });
                } else {
                    // if clickedTab is to the right of the activeTab

                    let self = this;

                    // animate activeTab to the right
                    $('#' + _activeTab.attr('data-tab-id')).animate({
                        left: '-' + contentWidth
                    }, {
                        duration: this.options.duration,
                        easing: this.options.easing,
                        complete: function() {
                            self.setTabsInactive(_activeTab);
                            self.clickDisabled = false;
                            $(this).css('left', '0');
                        }
                    });

                    // animate clickedTab to the left
                    $('#' + clickedTab.attr('data-tab-id')).css({
                        left: contentWidth
                    }).animate({
                        left: '0'
                    }, {
                        duration: this.options.duration,
                        easing: this.options.easing
                    });
                }
            }
        }

        return false;
    };

    Tabs.prototype.scroll = function (direction, event, paramObject) {
        let defaultParams = {
            steps: 2
        };

        let options = $.extend({}, defaultParams, paramObject || {});

        if (!this.clickDisabled) {

            this.clickDisabled = true;
            let scrollLeft = $('ul.tabs').scrollLeft();
            let index = 0;
            let self = this;

            this.element.find('ul.tabs').children('li').each(function () {
                if (scrollLeft >= (self.getPositionOfTab($(this)))
                    && scrollLeft <= (self.getPositionOfTab($(this)) + $(this).width())) {
                    index = $(this).index();
                    return false;
                }
            });

            if (direction === 'left') {
                if ((index - options.steps) < 0) {
                    index = 0;
                    $('.scroll-btn#button-scroll-left').addClass('disabled');
                    this.clickDisabled = false;
                } else {
                    index -= options.steps;
                    $('.scroll-btn#button-scroll-left').removeClass('disabled')
                }

            } else if (direction === 'right') {
                if ((index + options.steps) > parseInt(this.element.find('ul.tabs li').length - 1)) {
                    index = this.element.find('ul.tabs li').length - 1;
                    $('.scroll-btn#button-scroll-right').addClass('disabled');
                    this.clickDisabled = false;
                } else {
                    index += options.steps;
                    $('.scroll-btn#button-scroll-right').removeClass('disabled');
                }
            }

            this.element.find('ul.tabs').scrollTo(this.element.find('ul.tabs li').eq(index), this.options.duration, {
                easing: this.options.easing,
                onAfter: function () {
                    self.clickDisabled = false;
                }
            });

        }
        return false;
    };

    Tabs.prototype.scrollTo = function (tab, options) {
        this.element.find('ul.tabs').scrollTo(tab, this.options.duration, {
            easing: this.options.easing
        });
    };

    Tabs.prototype.setTabsInactive = function(tab) {
        if (tab) {
            tab.removeClass('active');
            $('#' + tab.attr('data-tab-id')).removeClass('active');
        }

        this.setColors();
    };

    Tabs.prototype.setTabActive = function (tab) {
        this.activeTab = tab.addClass('active');
        this.activeTab.addClass('active');
        this.activeTabContent = this.element.find('.tab-content#' + tab.attr('data-tab-id')).addClass('active');
        this.activeTabContent.addClass('active');

        if (this.useURL) {
            let url = window.location.href;
            if (url.indexOf('#') >= 0) {
                url = url.slice(0, url.indexOf('#'));
            }
            window.location.href = url + '#' + this.activeTab.attr('data-tab-id');
        }

        this.setColors();
    };

    Tabs.prototype.setColors = function() {
        // set backgroundColors
        this.element.children('ul').css('background', this.options.tabsBackground);
        this.element.children('.tab-content-container').css('background', this.options.contentBackground);

        // set font color and slider color
        this.element.find('#slider').css("background", this.options.activeColor);
        this.element.find("li").css("color", this.options.inactiveColor);
        this.element.find("li.active").css("color", this.options.activeColor);
    };

    Tabs.prototype.ripple = function (element, event) {

        // set buttons height and width
        let buttonHeight = 60;
        let buttonWidth = element.width() + 60;

        // make it round
        if(buttonWidth >= buttonHeight) {
            buttonHeight = buttonWidth;
        } else {
            buttonWidth = buttonHeight;
        }

        let x = event.pageX - element.offset().left - buttonWidth / 2;
        let y = event.pageY - element.offset().top - buttonHeight / 2;

        // prepent new ripple
        element.prepend('<span class="ripple"></span>');

        // animate
        element.find(".ripple").css({
            'width': buttonWidth + 'px',
            'height': buttonHeight + 'px',
            'top': y + 'px',
            'left': x + 'px',
            'background': this.options.activeColor
        }).addClass('rippleEffect');
    };

    Tabs.prototype.getPositionOfTab = function (tab) {
        let pos = 0;
        let self = this;

        this.element.find('ul').children('li').each(function() {
            if ($(this).attr('data-tab-id') === tab.attr('data-tab-id')) {
                return false;
            }

            pos += $(this).width() + 60;
        });

        return pos;
    };
} (jQuery));