# Material Tabs
The material-tabs API provides auto generated tabs with jQuery. 

## Get started

First you need to include the following into your HTML before the actual tab API:

```html
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>    
<script src="http://cdn.jsdelivr.net/jquery.scrollto/2.1.2/jquery.scrollTo.min.js" type="text/javascript"></script>
``` 

Furthermore you need to include the [easings.net](https://easings.net/) library. 

You can include the material-tabs library by using the following reference:

```html
<link ref="stylesheet" href="http://material-tabs.raphael-muesseler.de/material-tabs/css/tabs.css" />
<script type="text/javascript" src="http://material-tabs.raphael-muesseler.de/material-tabs/js/tabs.js"></script>
```

## Usage

You can easily get started with the following:

```javascript
$('#tabs').tabs( {} ); 
```

Without the correct HTML structure, this will do not a thing. So please add to your HTML code:

```html
<div id="tabs">
	<ul>
    	<li data-tab-id="one">One</li>
        <li data-tab-id="two">Two</li>
        ...
    </ul>
    <div class="tab-content-container">
    	<div class="tab-content" id="one">
        	<h2>One</h2>
        </div>
        <div class="tab-content" id="two">
        	<h2>Two</h2>
        </div>
        ...
    </div>
</div>
```

## Options

Of course you can adjust your tab. In the following I will list up every option that you can modify to style or create 
your own material tabs. 

| Option                    | Data type     | Default value         | Description                                   |
|---                        | ---           | ---                   | ---                                           |
| `tabsBackground`          | `String`      | `'none'`              | background color of the tabs (CSS style)      |
| `contentBackground`       | `String`      | `'none'`              | background color of tabs content (CSS style)  |
| `activeColor`             | `String`      | `'#444'`              | color of highlighted tab                      |
| `inactiveColor`           | `String`      | `'#BBB'`              | color of not-highlighted tabs                 |
| `animate`                 | `boolean`     | `true`                | adds animations to tabs when chaning if true  |
| `easing`                  | `String`      | `'easeInOutQuart'`    | type of animation (see [easings.net](                                                                                       https://easings.net/)                         |
| `duration`                |  `int`        | `1500`                | duration of animation                         |
| `keyUsage`                | `boolean`     | `true`                | allows usings arrow key to navigate if true   |
| `useURL`                  | `boolean`     | `true`                | uses URL to store active tab and used to navigate via URL to tab   |


## Authors

- Raphael Muesseler - *initial work* - [raphaemue](http://bitbucket.org/raphaelmue)
